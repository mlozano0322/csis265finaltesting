﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csis265final.Services
{
    public interface IQuoteService
    {
        string GetQuote();
    }
}
