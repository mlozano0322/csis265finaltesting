﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csis265final.Models
{
    public interface ISoftwareAppRepository
    {
        List<SoftwareApp> GetAll();
        SoftwareApp GetSoftwareAppById(int? id);
    }
}
