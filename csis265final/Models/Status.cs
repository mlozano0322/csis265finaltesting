﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csis265final.Models
{
    public enum Status
    {
        Open, Assigned, OnHold, Complete
    }
}
