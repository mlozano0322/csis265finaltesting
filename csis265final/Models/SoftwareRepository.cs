﻿using csis265final.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csis265final.Models
{
    public class SoftwareRepository : ISoftwareAppRepository
    {
        private ApplicationDbContext _context;

        public SoftwareRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public List<SoftwareApp> GetAll()
        {
            return _context.SoftwareApps.ToList();  
        }

        public SoftwareApp GetSoftwareAppById(int? id)
        {
            var softwareApp =  _context.SoftwareApps.SingleOrDefault(m => m.Id == id);

            return softwareApp;
        }
    }
}
