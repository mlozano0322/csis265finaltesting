﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace csis265final.Models
{
    public class Bug
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5)]
        [Display(Name = "Issue")]
        public string Issue { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 5)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Resolution")]
        public string Resolution { get; set; }

        [Display(Name = "Priority")]
        public Priority Priority { get; set; }

        [Display(Name = "Status")]
        public Status Status { get; set; }


        [DataType(DataType.DateTime)]
        [Display(Name = "Date Reported")]
        public DateTime DateReported { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date Due")]
        public DateTime DateDue { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date Completed")]
        public DateTime DateCompleted { get; set; }

        [Display(Name = "Reported By User")]
        public string ReportedByUser { get; set; }

        [Display(Name = "Assigned to User")]
        public string AssignedToUser { get; set; }


        [Display(Name = "Software App")]
        public SoftwareApp SoftwareApp { get; set; }



    }
}
