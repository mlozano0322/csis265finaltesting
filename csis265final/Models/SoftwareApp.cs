﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


//one to many
namespace csis265final.Models
{
    public class SoftwareApp
    {

        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Name { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 5)]
        public string Purpose { get; set; }

        public List<Bug> Bugs { get; set; }

        public SoftwareApp()
        {
            Bugs = new List<Bug>();
        }


    }
}
