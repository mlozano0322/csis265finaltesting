﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using csis265final.Data;
using csis265final.Models;

namespace csis265final.Controllers
{
    public class SoftwareAppController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ISoftwareAppRepository _softwareAppRepository;

        public SoftwareAppController(ApplicationDbContext context, ISoftwareAppRepository softwareAppRepository)
        {
            _context = context;
            _softwareAppRepository = softwareAppRepository;
        }

        // GET: SoftwareApp
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.SoftwareApps.ToListAsync());
        //}

        // GET: SoftwareApp (repowork)
        public async Task<IActionResult> Index()
        {
            var model = _softwareAppRepository.GetAll();

            return View(await _context.SoftwareApps.ToListAsync());
        }
                
        // GET: SoftwareApp/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var softwareApp = await _context.SoftwareApps
        //        .FirstOrDefaultAsync(m => m.Id == id);



        //    if (softwareApp == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(softwareApp);
        //}
        
        // GET: SoftwareApp/Details/5 repostuff
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareApp = _softwareAppRepository.GetSoftwareAppById(id);

            if (softwareApp == null)
            {
                return NotFound();
            }

            return View(softwareApp);
        }


        // GET: SoftwareApp/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SoftwareApp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Purpose")] SoftwareApp softwareApp)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softwareApp);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(softwareApp);
        }

        // GET: SoftwareApp/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareApp = await _context.SoftwareApps.FindAsync(id);
            if (softwareApp == null)
            {
                return NotFound();
            }
            return View(softwareApp);
        }

        // POST: SoftwareApp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Purpose")] SoftwareApp softwareApp)
        {
            if (id != softwareApp.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(softwareApp);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftwareAppExists(softwareApp.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(softwareApp);
        }

        // GET: SoftwareApp/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareApp = await _context.SoftwareApps
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareApp == null)
            {
                return NotFound();
            }

            return View(softwareApp);
        }

        // POST: SoftwareApp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var softwareApp = await _context.SoftwareApps.FindAsync(id);
            _context.SoftwareApps.Remove(softwareApp);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftwareAppExists(int id)
        {
            return _context.SoftwareApps.Any(e => e.Id == id);
        }
    }
}
