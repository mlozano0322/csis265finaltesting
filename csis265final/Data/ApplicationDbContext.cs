﻿using System;
using System.Collections.Generic;
using System.Text;
using csis265final.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace csis265final.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<SoftwareApp> SoftwareApps { get; set; }

        public DbSet<Bug> Bugs { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
