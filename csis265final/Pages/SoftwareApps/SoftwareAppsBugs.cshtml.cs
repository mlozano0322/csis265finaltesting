﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csis265final.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace csis265final.Pages.SoftwareApps
{
    public class SoftwareAppsBugsModel : PageModel
    {
        private IQuoteService _quoteService;

        public string Quote { get; set; }

        public SoftwareAppsBugsModel(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }
        public void OnGet()
        {
            Quote = _quoteService.GetQuote();
        }
    }
}