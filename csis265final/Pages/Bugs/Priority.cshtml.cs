﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csis265final.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace csis265final.Pages.Bugs
{
    public class PriorityModel : PageModel
    {
        private IQuoteService _quoteService;

        public string Quote { get; set; }

        public PriorityModel(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }
        public void OnGet()
        {
            Quote = _quoteService.GetQuote();
        }
    }
}