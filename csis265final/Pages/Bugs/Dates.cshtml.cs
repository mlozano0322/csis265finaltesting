﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csis265final.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace csis265final.Pages.Bugs
{
    public class DatesModel : PageModel
    {
        private IQuoteService _quoteService;

        

        public string Quote { get; set; }



        public DatesModel(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }
        public void OnGet(/*put in date stuff*/)
        {
            Quote = _quoteService.GetQuote();
        }
    }
}